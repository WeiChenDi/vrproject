﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LiquidVolumeFX;

public class FormatGas
{

    private static FormatGas _instance;
    public static FormatGas Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new FormatGas();
            }
            return _instance;
        }
    }
    public MonoBehaviour Mono;

    public void OnFormatGas(MaterialClass material, ContainerController container)
    {
        Transform containerTsm = container.transform;
        List<Transform> bubblingsTsmList;
        Transform surfaceBubbleTsm;
        LiquidVolume liquidVolumn = containerTsm.GetComponentInChildren<LiquidVolume>();
        GetBubblesTsm(containerTsm, out bubblingsTsmList, out surfaceBubbleTsm);
        containerTsm.gameObject.SetActive(true);
        ActiveBubbles(bubblingsTsmList, surfaceBubbleTsm, liquidVolumn, container);
        //for (int i = 0; i < bubblingsTsmList.Count; i++)
        //{
        //    bubblingsTsmList[i].gameObject.SetActive(true);
        //}
    }

    private void ActiveBubbles(List<Transform> bubbleTsmList, Transform surfaceTsmBubble, LiquidVolume liquidVolumn, ContainerController container)
    {
        Mono.StartCoroutine(BubblesTimer(bubbleTsmList, surfaceTsmBubble, liquidVolumn, container));
    }

    IEnumerator BubblesTimer(List<Transform> bubblingsTsmList, Transform surfaceBubbleTsm, LiquidVolume liquidVolumn, ContainerController container)
    {
        List<ParticleSystem> bubbleParticleSystemsList = new List<ParticleSystem>();
        ParticleSystem surfaceBubbleParticleSystem = surfaceBubbleTsm.GetComponent<ParticleSystem>();
        for (int i = 0; i < bubblingsTsmList.Count; i++)
        {
            ParticleSystem particle = bubblingsTsmList[i].GetComponent<ParticleSystem>();
            bubbleParticleSystemsList.Add(particle);
        }
        float reactTime = ChemicalReactioinHandler.Instance.ReactTime - 3.5f;
        const int frameCount = 100;
        float currentTime = 0f;
        float deltaTIme = reactTime / frameCount;
        float initHeight = container.ParticleHeight;
        while (true)
        {
            if(currentTime >= reactTime)
            {
                yield break;
            }
            for(int i = 0; i < bubblingsTsmList.Count; i++)
            {
                bubblingsTsmList[i].localScale = 
                    new Vector3(bubblingsTsmList[i].localScale.x, initHeight * liquidVolumn.level,
                    bubblingsTsmList[i].localScale.z);
            }
            currentTime += deltaTIme;
            if(currentTime < reactTime / 4)
            {
                for(int i = 0; i < bubblingsTsmList.Count; i++)
                {
                    if(!bubbleParticleSystemsList[i].isPlaying)
                    {
                        bubbleParticleSystemsList[i].Play();
                    }
                }
                yield return new WaitForSeconds(deltaTIme);
            }
            else if(currentTime > reactTime * 3 / 4 )
            {
                if (currentTime < reactTime * 10 / 12)
                {
                    for (int i = 0; i < (int)bubblingsTsmList.Count / 3; i++)
                    {
                        if (bubbleParticleSystemsList[i].isPlaying)
                        {
                            bubbleParticleSystemsList[i].Stop();
                        }
                    }
                    yield return new WaitForSeconds(deltaTIme);
                }
                else if(currentTime < reactTime * 11 / 12)
                {
                    for (int i = (int)bubblingsTsmList.Count / 3; i < (int)bubblingsTsmList.Count * 2 / 3; i++)
                    {
                        if (bubbleParticleSystemsList[i].isPlaying)
                        {
                            bubbleParticleSystemsList[i].Stop();
                        }
                    }
                    yield return new WaitForSeconds(deltaTIme);
                }
                else
                {
                    for (int i = (int)bubblingsTsmList.Count * 2 / 3; i < (int)bubblingsTsmList.Count; i++)
                    {
                        if (bubbleParticleSystemsList[i].isPlaying)
                        {
                            bubbleParticleSystemsList[i].Stop();
                        }
                    }
                    yield return new WaitForSeconds(deltaTIme);
                }
            }
            else
            {
                yield return new WaitForSeconds(deltaTIme);
            }
        }
    }

    private void GetBubblesTsm(Transform containerTsm, out List<Transform> bubblingsTsmList, out Transform surfaceBubbleTsm)
    {
        bubblingsTsmList = new List<Transform>();
        surfaceBubbleTsm = AppUtility.FindDeepChild(containerTsm.gameObject, "SurfaceBubble", true);
        Transform tmpTsm = AppUtility.FindDeepChild(containerTsm.gameObject, "Bubblings", true);
        int cnt = tmpTsm.childCount;
        for (int i = 0; i < cnt; i++)
        {
            bubblingsTsmList.Add(tmpTsm.GetChild(i));
        }
    }
}
