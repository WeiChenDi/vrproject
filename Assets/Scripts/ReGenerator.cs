﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReGenerator : MonoBehaviour
{
    #region Variables
    private ContainerController _container;
    private SolidContainerController _solidContainerController;
    private const string _beakerTag = "ReactionContainer";
    private const string _tweezersTag = "Tweezers";
    private const string _potionTag = "Potion";
    private const string _solidContainerTag = "SolidContainer";
    private Coroutine _async;
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Start()
    {
        if(gameObject.tag != _tweezersTag && gameObject.tag != _solidContainerTag)
            _container = GetComponent<ContainerController>();
        if(gameObject.tag == _solidContainerTag)
        {
            _solidContainerController = GetComponent<SolidContainerController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.parent != null)
        {
            if (collision.transform.root.name == "Room" || collision.transform.name == "WasteCylinder")
            {
                if (_async != null)
                {
                    StopCoroutine(_async);
                }
                _async = StartCoroutine(OnCollideFloor());
            }
        }
    }
    #endregion

    #region Custom Methods
    IEnumerator OnCollideFloor()
    {
        yield return new WaitForSeconds(0.5f);
        if (gameObject.tag == _beakerTag)
        {
            ContainerGeneratorController.Instance.GenerateContainer(_beakerTag, MaterialType.None);
        }
        else if (gameObject.tag == _tweezersTag)
        {
            ContainerGeneratorController.Instance.GenerateContainer(_tweezersTag, MaterialType.None);
        }
        else if(gameObject.tag == _potionTag)
        {
            if (_container.materialTypes.Count != 1)
            {
                Debug.LogError("Potion containerController in wrong format!!!");
            }
            ContainerGeneratorController.Instance.GenerateContainer(_potionTag, _container.materialTypes[0]);
        }
        else if(gameObject.tag == _solidContainerTag)
        {
            ContainerGeneratorController.Instance.GenerateContainer(_solidContainerTag, _solidContainerController.type);
        }
        Destroy(gameObject);
        _async = null;
    }
    #endregion
}
