﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadController : MonoBehaviour
{
    #region Variables
    public Material PadNormalMat;
    public Material PadHighLightMat;
    public Material PadErrorMat;
    public Material PadCorrectMat;
    public MaterialType Type;

    private const string _potionTag = "Potion";
    private const string _solidContainerTag = "SolidContainer";

    private bool _placed = false;
    public bool Placed
    {
        get
        {
            return _placed;
        }
    }
    private ContainerController _curContainer;
    private SolidContainerController _curSolidContainer;
    private GameObject _curContainerObj;
    private MaterialType _currentPlacedType = MaterialType.None;
    public MaterialType CurrentPlacedType
    {
        get
        {
            return _currentPlacedType;
        }
    }
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if((other.gameObject.tag == _potionTag || other.gameObject.tag == _solidContainerTag) && !_placed)
        {
            _placed = true;
            _curContainerObj = other.gameObject;
            if(other.gameObject.tag == _potionTag)
            {
                _curContainer = other.gameObject.GetComponent<ContainerController>();
                if (_curContainer == null)
                {
                    Debug.LogError("ContainerController in wrong Format!!!");
                }
                if (_curContainer.materialTypes.Count != 1)
                {
                    Debug.LogError("MaterialType of containerController in wrong Format!!!");
                }
                _currentPlacedType = _curContainer.materialTypes[0];
                SetHighLightMaterial();
            }
            else if(other.gameObject.tag == _solidContainerTag)
            {
                _curSolidContainer = other.gameObject.GetComponent<SolidContainerController>();
                if(_curSolidContainer == null)
                {
                    Debug.LogError("SolidContainerController in wrong Format!!!");
                }
                _currentPlacedType = _curSolidContainer.type;
                SetHighLightMaterial();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if((other.gameObject.tag == _potionTag || other.gameObject.tag == _solidContainerTag) && other.gameObject == _curContainerObj)
        {
            _placed = false;
            _curContainer = null;
            _curContainerObj = null;
            _currentPlacedType = MaterialType.None;
            SetNormalMaterial();
        }
    }
    #endregion

    #region Custom Methods
    private void SetHighLightMaterial()
    {
        GetComponent<Renderer>().material = PadHighLightMat;
    }

    private void SetNormalMaterial()
    {
        GetComponent<Renderer>().material = PadNormalMat;
    }

    public void SetErrorMaterial()
    {
        GetComponent<Renderer>().material = PadErrorMat;
    }

    public void SetCorrectMaterial()
    {
        GetComponent<Renderer>().material = PadCorrectMat;
    }
    #endregion
}
