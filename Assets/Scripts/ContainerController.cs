﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;
using LiquidVolumeFX;

public class ContainerController : MonoBehaviour
{
    #region Variables
    private ObiEmitter _emitter;
    private ObiSolver _solver;
    private GameObject _vrCameraObjLeft;
    private GameObject _vrCameraObjRight;
    private ObiFluidRenderer _fluidRenderer1;
    private ObiFluidRenderer _fluidRenderer2;
    private ObiParticleRenderer _particleRenderer;
    private MeshCollider _collider;
    private const string _reactionContainerTag = "ReactionContainer";
    private LiquidVolume _liquidVolume;
    public LiquidVolume LiquidVolume
    {
        get
        {
            return _liquidVolume;
        }
    }
    public ObiEmitterMaterial lowResolutionMat;
    private Coroutine _async;

    private float _particleHeight;
    public float ParticleHeight
    {
        get
        {
            return _particleHeight;
        }
    }
    // 存储容器中的沉淀物信息
    private List<Sediment> _sedimentsList = new List<Sediment>();
    public List<Sediment> SedimentsList
    {
        get
        {
            return _sedimentsList;
        }
        set
        {
            _sedimentsList = value;
        }
    }
    public bool IsReacting = false;

    // 存储容器中所有共存的物质种类
    public List<MaterialType> materialTypes = new List<MaterialType>();
    #endregion

    #region Main Methods
    private void Awake()
    {
        _collider = gameObject.GetComponent<MeshCollider>();
        if (_collider == null)
        {
            Debug.LogError("Error: Component MeshCollider is needed!");
        }
        _liquidVolume = gameObject.GetComponentInChildren<LiquidVolume>();
        if (_liquidVolume == null)
        {
            Debug.LogError("Error: Component LiquidVolume is needed!");
        }
        LiquidInContainerMgr.Instance.ContainerLiquidDic.Add(_collider, _liquidVolume);
        _particleRenderer = (ObiParticleRenderer)GetComponentInChildren(typeof(ObiParticleRenderer), true);
        if (_particleRenderer == null)
        {
            Debug.LogError("Error: Component ObiParticleRenderer is needed!");
        }
        AddToCollistionHandler();
    }
    // Start is called before the first frame update
    void Start()
    {
        _emitter = (ObiEmitter)gameObject.GetComponentInChildren(typeof(ObiEmitter), true);
        if (_emitter == null)
        {
            Debug.LogError("Error: Component ObiEmitter is needed!");
        }
        _vrCameraObjLeft = GameObject.Find("CamLeft (origin)");
        _fluidRenderer1 = _vrCameraObjLeft.transform.Find("CamLeft (eye)").GetComponent<ObiFluidRenderer>();
        if (_fluidRenderer1 == null)
        {
            Debug.LogError("Error: Component ObiFluidRenderer is needed!");
        }
        _vrCameraObjRight = GameObject.Find("CamRight (origin)");
        _fluidRenderer2 = _vrCameraObjRight.transform.Find("CamRight (eye)").GetComponent<ObiFluidRenderer>();
        if (_fluidRenderer2 == null)
        {
            Debug.LogError("Error: Component ObiFluidRenderer is needed!");
        }
        GameObject solverObj = GameObject.Find("Solver");
        _solver = solverObj.GetComponent<ObiSolver>();
        if(_solver == null)
        {
            Debug.LogError("Error: Component ObiSolver is needed!");
        }
        AddSolver();
        AddToParticleRenderer();
        
        SetEmitterMat();
        if (gameObject.tag == "ReactionContainer")
        {
            Transform bubblingsTsm = AppUtility.FindDeepChild(gameObject, "Bubblings");
            if(bubblingsTsm.childCount != 0)
            {
                _particleHeight = bubblingsTsm.GetChild(0).transform.localScale.y;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if (tag == "ReactionContainer")
        //{
        //    Debug.Log(_liquidVolume.liquidColor1);
        //}
        //if(!IsReacting)
        //{
        //    ChemicalReactioinHandler.Instance.CheckReact(gameObject);
        //}
        if (gameObject.tag == _reactionContainerTag)
        {
            if(materialTypes.Contains(MaterialType.Fe_OH_2) && !IsReacting)
            {
                materialTypes.Remove(MaterialType.Fe_OH_2);
                ChemicalReactioinHandler.Instance.Fe_OH_2React(gameObject);
            }
            if (materialTypes.Contains(MaterialType.NaOH) && !IsReacting)
            {
                ChemicalReactioinHandler.Instance.CheckReact(gameObject);
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(gameObject.tag == _reactionContainerTag)
        {
            if(other.gameObject.tag == "Sediment")
            {
                other.transform.parent = transform;
                SolidObjController sediment = other.GetComponent<SolidObjController>();
                AddSediment(sediment.SedimentInfo);
            }
            else if (other.gameObject.tag == "PhPaper")
            {
                PhPaper phPaper = other.gameObject.GetComponent<PhPaper>();
                phPaper.PhTest(materialTypes);
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (gameObject.tag == "ReactionContainer")
        {
            if (other.gameObject.tag == "Sediment")
            {
                other.transform.parent = null;
                SolidObjController sediment = other.GetComponent<SolidObjController>();
                RemoveSediment(sediment.SedimentInfo);
            }
        }
    }
    #endregion

    #region Custom Methods
    private void AddToParticleRenderer()
    {
        List<ObiParticleRenderer> tmpList;
        ArrayToList<ObiParticleRenderer>(out tmpList, _fluidRenderer1.particleRenderers);
        List<ObiParticleRenderer> tmpList1;
        ArrayToList<ObiParticleRenderer>(out tmpList1, _fluidRenderer2.particleRenderers);
        tmpList.Add(_particleRenderer);
        ObiParticleRenderer[] tmpArray;
        ObiParticleRenderer[] tmpArray1;
        ListToArray<ObiParticleRenderer>(tmpList, out tmpArray);
        ListToArray<ObiParticleRenderer>(tmpList, out tmpArray1);
        _fluidRenderer1.particleRenderers = tmpArray;
        _fluidRenderer2.particleRenderers = tmpArray1;
    }

    private void AddSolver()
    {
        _emitter.Solver = _solver;
        _emitter.enabled = true;
    }

    private void AddToCollistionHandler()
    {
        CollisionHandler.Instance.killers.Add(_collider);
    }

    private void ArrayToList<T>(out List<T> list, T[] array)
    {
        list = new List<T>(array);
    }

    private void ListToArray<T>(List<T> list, out T[] array)
    {
        array = list.ToArray();
    }

    private void SetEmitterMat()
    {
        _emitter.EmitterMaterial = lowResolutionMat;
    }

    private void AddSediment(Sediment sediment)
    {
        _sedimentsList.Add(sediment);
        if(!materialTypes.Contains(sediment.material.type))
        {
            materialTypes.Add(sediment.material.type);
        }
        ChemicalReactioinHandler.Instance.CheckReact(gameObject);
    }

    private void RemoveSediment(Sediment sediment)
    {
        if(_sedimentsList.Contains(sediment))
        {
            _sedimentsList.Remove(sediment);
        }
        for(int i = 0; i < _sedimentsList.Count; i++)
        {
            if(_sedimentsList[i].material == sediment.material)
            {
                return;
            }
        }
        if(materialTypes.Contains(sediment.material.type))
        {
            materialTypes.Remove(sediment.material.type);
        }
    }

    public void SetMaterialType(MaterialType type)
    {
        if(materialTypes.Count != 0)
        {
            Debug.LogError("Wrong ContainerController Format!!!");
            return;
        }
        materialTypes.Add(type);
    }

    public void SetLiquidVolumeColor(MaterialType type)
    {
        if(type == MaterialType.None)
        {
            _liquidVolume.liquidColor1 = ColorConfigMgr.Instance.GetColor(ColorType.White, MaterialState.Liquid);
            _liquidVolume.liquidColor2 = ColorConfigMgr.Instance.GetColor(ColorType.White, MaterialState.Liquid);
            return;
        }
        MaterialClass material = MaterialConfigMgr.Instance.materialsDic[type];

        _liquidVolume.liquidColor1 = ColorConfigMgr.Instance.GetColor(material.color, material.state);
        _liquidVolume.liquidColor2 = ColorConfigMgr.Instance.GetColor(material.color, material.state);
    }

    public void SetLiquidVolumeColor(Color colorType)
    {
        _liquidVolume.liquidColor1 = colorType;
        _liquidVolume.liquidColor2 = colorType;
    }

    public void SetParticleRendererColor(MaterialType type)
    {
        if (type == MaterialType.None)
        {
            _particleRenderer.particleColor = ColorConfigMgr.Instance.GetColor(ColorType.White, MaterialState.Liquid);
            return;
        }
        MaterialClass material = MaterialConfigMgr.Instance.materialsDic[type];
        _particleRenderer.particleColor = ColorConfigMgr.Instance.GetColor(material.color, material.state);
    }

    public void SetParticleRendererColor(Color colorType)
    {
        _particleRenderer.particleColor = colorType;
    }

    public void ChangeLiquidVolumeColor(Color targetColor)
    {
        if(materialTypes.Count == 0)
        {
            SetLiquidVolumeColor(targetColor);
            SetParticleRendererColor(targetColor);
            return;
        }
        int i = 0;
        for(i = 0; i < materialTypes.Count; i++)
        {
            if(MaterialConfigMgr.Instance.materialsDic[materialTypes[i]].state == MaterialState.Liquid)
            {
                break;
            }
        }
        if(i == materialTypes.Count)
        {
            SetLiquidVolumeColor(targetColor);
            SetParticleRendererColor(targetColor);
            return;
        }
        Color oldColor = _liquidVolume.liquidColor1;

        if (_async != null)
        {
            StopCoroutine(_async);
        }
        _async = StartCoroutine(StartChangingColorSteedy(oldColor, targetColor));
    }

    // 当需要一点一点混合液体的时候调用
    IEnumerator StartChangingColorSteedy(Color oldColor, Color newColor)
    {
        Color changeColor = newColor - oldColor;
        changeColor.r /= 4;
        changeColor.g /= 4;
        changeColor.b /= 4;
        changeColor.a /= 4;
        StartCoroutine(ChangingLiquidVolumeColor(oldColor, oldColor + changeColor));
        yield return new WaitForSeconds(0.5f);
        _async = null;
    }

    public void StartChangingColor(Color oldColor, Color newColor)
    {
        StartCoroutine(StartChangingColorDirectly(oldColor, newColor));
    }

    // 当需要反应变化颜色的时候调用
    IEnumerator StartChangingColorDirectly(Color oldColor, Color newColor)
    {
        StartCoroutine(ChangingLiquidVolumeColor(oldColor, newColor));
        yield return new WaitForSeconds(0.5f);
        _async = null;
    }

    IEnumerator ChangingLiquidVolumeColor(Color oldColor, Color newColor)
    {
        float lastLength = 3f;
        const int framesCount = 100;
        float deltaTime = lastLength / framesCount;
        float deltaR = (newColor.r - oldColor.r) / framesCount;
        float deltaG = (newColor.g - oldColor.g) / framesCount;
        float deltaB = (newColor.b - oldColor.b) / framesCount;
        float deltaA = (newColor.a - oldColor.a) / framesCount;
        Color currentColor = oldColor;
        float currentTime = 0f;
        while (true)
        {
            if(currentTime >= lastLength)
            {
                yield break;
            }
            currentColor.r += deltaR;
            currentColor.g += deltaG;
            currentColor.b += deltaB;
            currentColor.a += deltaA;
            _liquidVolume.liquidColor1 = currentColor;
            _liquidVolume.liquidColor2 = currentColor;
            currentTime += deltaTime;
            yield return new WaitForSeconds(deltaTime);
        }
    }
    #endregion
}
