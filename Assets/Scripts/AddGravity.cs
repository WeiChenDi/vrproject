﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddGravity : MonoBehaviour
{
    #region Variables
    private Rigidbody _rigid;
    public bool useGravity = true;
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Start()
    {
        _rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _rigid.useGravity = false;
        if(transform.tag == "Sediment")
        {
            if(transform.parent != null)
            {
                if (transform.parent.tag == "ReactionContainer")
                {
                    Vector3 containerMeshSize;
                    GetContainerMeshSize(out containerMeshSize, transform.parent);
                    float height = GetLiquidLevel(transform.parent, containerMeshSize.y);
                    if (transform.localPosition.y <= height)
                    {
                        _rigid.mass = 0.2f;
                    }
                    else
                    {
                        _rigid.mass = 1f;
                    }
                }
                else
                {
                    _rigid.mass = 1f;
                }
            }
            else
            {
                _rigid.mass = 1f;
            }
        }
        if(useGravity)
        {
            _rigid.AddForce(Physics.gravity * (_rigid.mass * _rigid.mass));
        }
    }
    #endregion

    #region Custom Methods
    private void GetContainerMeshSize(out Vector3 meshSize, Transform containerTsm)
    {
        MeshFilter meshFilter = containerTsm.GetComponent<MeshFilter>();
        meshSize = new Vector3();
        meshSize.x = meshFilter.mesh.bounds.size.x * containerTsm.localScale.x;
        meshSize.y = meshFilter.mesh.bounds.size.y * containerTsm.localScale.y;
        meshSize.z = meshFilter.mesh.bounds.size.z * containerTsm.localScale.z;
    }

    private float GetLiquidLevel(Transform containerTsm, float containerHeight)
    {
        LiquidVolumeFX.LiquidVolume volumn = (LiquidVolumeFX.LiquidVolume)containerTsm.GetComponentInChildren(typeof(LiquidVolumeFX.LiquidVolume));
        return volumn.level * containerHeight;
    }
    #endregion
}
