﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerGeneratorController : MonoBehaviour
{
    #region Variables
    private const string _beakerTag = "ReactionContainer";
    private const string _tweezersTag = "Tweezers";
    private const string _potionTag = "Potion";
    private const string _solidContainerTag = "SolidContainer";
    private const string _phIndicatorTag = "PhIndicatorPaper";

    private const string _potionPath = "Prefabs/Potion";
    private const string _beakerPath = "Prefabs/Beaker";
    private const string _tweezersPath = "Prefabs/Tweezers";
    private const string _solidContainerPath = "Prefabs/SolidContainer";
    private const string _phIndicatorPaperPath = "Prefabs/PhIndicatorPaper";

    private GameObject _potionPre;
    private GameObject _beakerPre;
    private GameObject _tweezersPre;
    private GameObject _solidContainerPre;
    private GameObject _phIndicatorPaperPre;

    private List<GeneratePointClass> _generatePointsList = new List<GeneratePointClass>();

    private static ContainerGeneratorController _instance;
    public static ContainerGeneratorController Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.Find("ContainerGenerator").GetComponent<ContainerGeneratorController>();
            }
            return _instance;
        }
    }
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Awake()
    {
        InitGeneratePointsList();
        InitContainerPrefabs();
        InitContainers();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion

    #region Custom Methods
    private void InitContainerPrefabs()
    {
        _potionPre = Resources.Load(_potionPath) as GameObject;
        _beakerPre = Resources.Load(_beakerPath) as GameObject;
        _tweezersPre = Resources.Load(_tweezersPath) as GameObject;
        _solidContainerPre = Resources.Load(_solidContainerPath) as GameObject;
        _phIndicatorPaperPre = Resources.Load(_phIndicatorPaperPath) as GameObject;

    }

    private void InitGeneratePointsList()
    {
        int pointsNum = transform.childCount;
        List<Transform> generatePointsTsms = new List<Transform>();
        for (int i = 0; i < pointsNum; i++)
        {
            Transform tmpTsm = transform.GetChild(i);
            GeneratePoint generatePoint = tmpTsm.GetComponent<GeneratePoint>();
            _generatePointsList.Add(generatePoint.generatePointInfo);
        }
    }

    private void InitContainers()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            GenerateContainer(_generatePointsList[i].tagName, _generatePointsList[i].type);
        }
    }

    public void GenerateContainer(string tagName, MaterialType type)
    {
        // 要生成的是potion
        if(tagName == _potionTag)
        {
            GeneratePointClass generatePointClass = new GeneratePointClass();
            for(int i = 0; i < _generatePointsList.Count; i++)
            {
                if(_generatePointsList[i].type == type)
                {
                    generatePointClass = _generatePointsList[i];
                }
            }
            GameObject containerObj = InstaniateContainer(generatePointClass, tagName);
            ContainerController container = containerObj.GetComponent<ContainerController>();
            container.SetMaterialType(type);
            // 根据物质类型设置颜色
            container.SetLiquidVolumeColor(type);
            container.SetParticleRendererColor(type);
        }
        // 要生成的是beaker
        else if(tagName == _beakerTag)
        {
            GeneratePointClass generatePointClass = new GeneratePointClass();
            for (int i = 0; i < _generatePointsList.Count; i++)
            {
                if (_generatePointsList[i].tagName == tagName)
                {
                    generatePointClass = _generatePointsList[i];
                }
            }
            GameObject containerObj = InstaniateContainer(generatePointClass, tagName);
            ContainerController container = containerObj.GetComponent<ContainerController>();
            container.SetLiquidVolumeColor(MaterialType.None);
            container.SetParticleRendererColor(MaterialType.None);
        }
        // 要生成的是tweezer
        else if (tagName == _tweezersTag)
        {
            GeneratePointClass generatePointClass = new GeneratePointClass();
            for (int i = 0; i < _generatePointsList.Count; i++)
            {
                if (_generatePointsList[i].tagName == tagName)
                {
                    generatePointClass = _generatePointsList[i];
                }
            }
            GameObject containerObj = InstaniateContainer(generatePointClass, tagName);

        }
        else if(tagName == _solidContainerTag)
        {
            GeneratePointClass generatePointClass = new GeneratePointClass();
            for (int i = 0; i < _generatePointsList.Count; i++)
            {
                if (_generatePointsList[i].type == type)
                {
                    generatePointClass = _generatePointsList[i];
                }
            }
            GameObject containerObj = InstaniateContainer(generatePointClass, tagName);
            SetSolidContainerColor(containerObj, type);
            SolidContainerController solidContainerController = containerObj.GetComponent<SolidContainerController>();
            solidContainerController.type = type;
        }
        else if(tagName == _phIndicatorTag)
        {
            GeneratePointClass generatePointClass = new GeneratePointClass();
            for (int i = 0; i < _generatePointsList.Count; i++)
            {
                if (_generatePointsList[i].tagName == tagName)
                {
                    generatePointClass = _generatePointsList[i];
                }
            }
            GameObject containerObj = InstaniateContainer(generatePointClass, tagName);
        }
    }

    private GameObject InstaniateContainer(GeneratePointClass generatePointClass, string tagName)
    {
        GameObject containerObj = null;
        if (tagName == _beakerTag)
        {
            containerObj = Instantiate(_beakerPre, generatePointClass.position, Quaternion.identity, generatePointClass.generatePointObj.transform);
        }
        else if(tagName == _tweezersTag)
        {
            containerObj = Instantiate(_tweezersPre, generatePointClass.position, Quaternion.identity, generatePointClass.generatePointObj.transform);
        }
        else if(tagName == _solidContainerTag)
        {
            containerObj = Instantiate(_solidContainerPre, generatePointClass.position, Quaternion.identity, generatePointClass.generatePointObj.transform);
        }
        else if(tagName == _potionTag)
        {
            containerObj = Instantiate(_potionPre, generatePointClass.position, Quaternion.identity, generatePointClass.generatePointObj.transform);
        }
        else if(tagName == _phIndicatorTag)
        {
            containerObj = Instantiate(_phIndicatorPaperPre, generatePointClass.position, Quaternion.identity, generatePointClass.generatePointObj.transform);
        }

        if(containerObj != null)
        {
            containerObj.transform.position = generatePointClass.position;
        }
        return containerObj;
    }

    private void SetSolidContainerColor(GameObject containerObj ,MaterialType type)
    {
        List<GameObject> stoneList = new List<GameObject>();
        for(int i = 0; i < containerObj.transform.childCount; i++)
        {
            stoneList.Add(containerObj.transform.GetChild(i).gameObject);
        }
        MaterialClass materialClass = MaterialConfigMgr.Instance.materialsDic[type];
        Material mat = ColorConfigMgr.Instance.SolidMatDic[materialClass.color];
        for(int i = 0; i < stoneList.Count; i++)
        {

            stoneList[i].GetComponent<Renderer>().material = mat;
        }
    }
    #endregion
}

[System.Serializable]
public class GeneratePointClass
{
    public GameObject generatePointObj;
    public Vector3 position = new Vector3(0, 0, 0);
    public string tagName = string.Empty;
    public MaterialType type = MaterialType.None;
}
