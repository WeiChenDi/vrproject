﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidObjController : MonoBehaviour
{
    #region Variables
    private Sediment _sedimentInfo;
    public Sediment SedimentInfo
    {
        get
        {
            return _sedimentInfo;
        }
    }
    private bool _inContainer;
    public bool InContainer
    {
        get
        {
            return _inContainer;
        }
    }
    private Renderer _renderer;
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Start()
    {
        Physics.gravity = new Vector3(0, -1f, 0);
        _renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "ReactionContainer" && transform.parent != collision.transform)
        {
            ContactPoint contact = collision.contacts[0];
            //if(transform.parent != collision.transform)
            //{
            //    transform.parent = collision.transform;
            //}
            Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
            Vector3 pos = contact.point;
            ContainerController container = collision.gameObject.GetComponent<ContainerController>();
        }
    }
    #endregion

    #region Custom Methods
    public void SetSedimentInfo(Sediment sediment)
    {
        if(_sedimentInfo == null)
        {
            _sedimentInfo = new Sediment();
        }
        _sedimentInfo = sediment;
    }

    public void StartChangingColor(Color newColor)
    {
        StartCoroutine(ChangingLiquidVolumeColor(_renderer.material.color, newColor));
    }

    IEnumerator ChangingLiquidVolumeColor(Color oldColor, Color newColor)
    {
        float lastLength = 3f;
        const int framesCount = 100;
        float deltaTime = lastLength / framesCount;
        float deltaR = (newColor.r - oldColor.r) / framesCount;
        float deltaG = (newColor.g - oldColor.g) / framesCount;
        float deltaB = (newColor.b - oldColor.b) / framesCount;
        float deltaA = (newColor.a - oldColor.a) / framesCount;
        Color currentColor = oldColor;
        float currentTime = 0f;
        while (true)
        {
            if (currentTime >= lastLength)
            {
                yield break;
            }
            currentColor.r += deltaR;
            currentColor.g += deltaG;
            currentColor.b += deltaB;
            currentColor.a += deltaA;
            _renderer.material.color = currentColor;
            currentTime += deltaTime;
            yield return new WaitForSeconds(deltaTime);
        }
    }
    #endregion
}
