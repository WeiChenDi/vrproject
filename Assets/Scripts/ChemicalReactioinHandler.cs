﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ChemicalReactioinHandler : MonoBehaviour
{
    #region Variables
    private static ChemicalReactioinHandler _instance;
    public static ChemicalReactioinHandler Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.Find("ChemicalReactionHandler").GetComponent<ChemicalReactioinHandler>();
            }
            return _instance;
        }
    }

    private List<ChemistryReaction> _chemistryReactionsList = new List<ChemistryReaction>();
    public List<ChemistryReaction> chemistryReactionsList
    {
        get
        {
            return _chemistryReactionsList;
        }
        set
        {
            _chemistryReactionsList = value;
        }
    }

    private const float _reactTime = 5f;
    public float ReactTime
    {
        get
        {
            return _reactTime;
        }
    }
    private Coroutine _coroutine;
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Start()
    {
        // MaterialConfigMgr.Instance.InitConfig();
        ChemistryReactionConfigMgr.Instance.InitConfig();
        FormatSediment.Mono = this;
        FormatGas.Instance.Mono = this;
        DissolveSediment.Instance.Mono = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion

    #region Custom Methods
    public void MaterialMixing(GameObject srcContainerObj, GameObject dstContainerObj)
    {
        ContainerController srcContainer = srcContainerObj.GetComponent<ContainerController>();
        ContainerController dstContainer = dstContainerObj.GetComponent<ContainerController>();
        if (!dstContainer.IsReacting)
        {
            dstContainer.ChangeLiquidVolumeColor(srcContainer.LiquidVolume.liquidColor1);
            for (int i = 0; i < srcContainer.materialTypes.Count; i++)
            {
                if (!dstContainer.materialTypes.Contains(srcContainer.materialTypes[i]))
                {
                    dstContainer.materialTypes.Add(srcContainer.materialTypes[i]);
                    dstContainer.materialTypes.Add(MaterialType.H2O);
                }
            }
            CheckIfReact(dstContainer);
        }
    }

    public void CheckReact(GameObject dstContainerObj)
    {
        ContainerController dstContainer = dstContainerObj.GetComponent<ContainerController>();
        if (!dstContainer.IsReacting)
        {
            CheckIfReact(dstContainer);
        }
    }

    private void CheckIfReact(ContainerController container)
    {
        List<MaterialClass> materialClassList = new List<MaterialClass>();
        for(int i = 0; i < container.materialTypes.Count; i++)
        {
            materialClassList.Add(MaterialConfigMgr.Instance.materialsDic[container.materialTypes[i]]);
        }
        for (int i = 0; i < _chemistryReactionsList.Count; i++)
        {
            if (CheckLists(_chemistryReactionsList[i], materialClassList))
            {
                React(container, _chemistryReactionsList[i]);
            }
        }
    }

    private bool CheckLists(ChemistryReaction reaction, List<MaterialClass> materialList)
    {
        for(int i = 0; i < reaction.reactant.Count; i++)
        {
            if(materialList.Contains(reaction.reactant[i]))
            {
                continue;
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    private void React(ContainerController container, ChemistryReaction reaction)
    {
        OnReactStart(container, reaction);
        if(_coroutine != null)
        {
            StopCoroutine(_coroutine);
        }
        _coroutine = StartCoroutine(Timer(container, reaction));
    }

    // 计时器，设定反应时间为5秒
    IEnumerator Timer(ContainerController container, ChemistryReaction reaction)
    {
        while(true)
        {
            yield return new WaitForSeconds(_reactTime);
            OnReactStop(container, reaction);
        }
    }

    private void OnReactStart(ContainerController container, ChemistryReaction reaction)
    {
        Debug.Log("反应开始");
        container.IsReacting = true;
        // 更新物质
        // 生成物现象
        for (int i = 0; i < reaction.product.Count; i++)
        {
            // 生成沉淀
            if(reaction.product[i].state == MaterialState.Solid)
            {
                FormatSediment formatSediment = new FormatSediment();
                FormatSediment.Instance.OnFormatSediment(reaction.product[i], container);
            }
            // 生成气体
            if(reaction.product[i].state == MaterialState.Gas)
            {
                FormatGas.Instance.OnFormatGas(reaction.product[i], container);
            }
        }
        // 改变液体颜色
        List<ColorType> materialColorsList = new List<ColorType>();
        for (int j = 0; j < container.materialTypes.Count; j++)
        {
            if (reaction.reactant.Contains(MaterialConfigMgr.Instance.materialsDic[container.materialTypes[j]])
                || MaterialConfigMgr.Instance.materialsDic[container.materialTypes[j]].state == MaterialState.Gas
                || MaterialConfigMgr.Instance.materialsDic[container.materialTypes[j]].state == MaterialState.Solid
                || MaterialConfigMgr.Instance.materialsDic[container.materialTypes[j]].color == ColorType.White)
            {
                continue;
            }
            materialColorsList.Add(MaterialConfigMgr.Instance.materialsDic[container.materialTypes[j]].color);
        }
        for(int i = 0; i < reaction.product.Count; i++)
        {
            if(reaction.product[i].state == MaterialState.Gas || reaction.product[i].state == MaterialState.Solid
                || reaction.product[i].type == MaterialType.H2O)
            {
                continue;
            }
            materialColorsList.Add(reaction.product[i].color);
        }
        float factor = 1f / materialColorsList.Count;
        Color newColor = new Color();
        for (int j = 0; j < materialColorsList.Count; j++)
        {
            newColor.r = ColorConfigMgr.Instance.GetColor(materialColorsList[j], MaterialState.Liquid).r * factor;
            newColor.g = ColorConfigMgr.Instance.GetColor(materialColorsList[j], MaterialState.Liquid).g * factor;
            newColor.b = ColorConfigMgr.Instance.GetColor(materialColorsList[j], MaterialState.Liquid).b * factor;
            newColor.a = ColorConfigMgr.Instance.GetColor(materialColorsList[j], MaterialState.Liquid).a * factor;
        }
        container.StartChangingColor(container.LiquidVolume.liquidColor1, newColor);
        // 反应物现象
        for (int i = 0; i < reaction.reactant.Count; i++)
        {
            if(reaction.reactant[i].state == MaterialState.Solid)
            {
                if(reaction.dissolutionSolid.IsSoluble)
                {
                    DissolveSediment.Instance.OnDissolveSediment(reaction.dissolutionSolid.types, container);
                }
            }
        }
    }

    private void OnReactStop(ContainerController container, ChemistryReaction reaction)
    {
        Debug.Log("反应结束");
        UpdateMaterial(container, reaction);
        container.IsReacting = false;
        StopCoroutine(_coroutine);
    }

    private void UpdateMaterial(ContainerController container, ChemistryReaction reaction)
    {
        for (int i = 0; i < reaction.reactant.Count; i++)
        {
            if (container.materialTypes.Contains(reaction.reactant[i].type))
            {
                container.materialTypes.Remove(reaction.reactant[i].type);
            }
        }
        for (int i = 0; i < reaction.product.Count; i++)
        {
            if (!container.materialTypes.Contains(reaction.product[i].type))
            {
                container.materialTypes.Add(reaction.product[i].type);
            }
        }
    }

    public static bool CompareLists<T>(List<T> aListA, List<T> aListB)
    {
        if (aListA == null || aListB == null || aListA.Count != aListB.Count)
            return false;
        if (aListA.Count == 0)
            return true;
        Dictionary<T, int> lookUp = new Dictionary<T, int>();
        for (int i = 0; i < aListA.Count; i++)
        {
            int count = 0;
            if (!lookUp.TryGetValue(aListA[i], out count))
            {
                lookUp.Add(aListA[i], 1);
                continue;
            }
            lookUp[aListA[i]] = count + 1;
        }
        for (int i = 0; i < aListB.Count; i++)
        {
            int count = 0;
            if (!lookUp.TryGetValue(aListB[i], out count))
            {
                return false;
            }
            count--;
            if (count <= 0)
                lookUp.Remove(aListB[i]);
            else
                lookUp[aListB[i]] = count;
        }
        return lookUp.Count == 0;
    }

    public void Fe_OH_2React(GameObject containerObj)
    {
        ContainerController container = containerObj.GetComponent<ContainerController>();
        Debug.Log("硫酸亚铁反应");
        List<GameObject> rockList = new List<GameObject>();
        for(int i = 0; i < containerObj.transform.childCount; i++)
        {
            if(containerObj.transform.GetChild(i).name.Contains("Rock"))
            {
                SolidObjController solidObjContainer = containerObj.transform.GetChild(i).GetComponent<SolidObjController>();
                Sediment sediment = solidObjContainer.SedimentInfo;
                if(sediment.material.type == MaterialType.Fe_OH_2)
                {
                    solidObjContainer.StartChangingColor(ColorConfigMgr.Instance.SolidMatDic[ColorType.Red_Brown].color);
                    sediment.material = MaterialConfigMgr.Instance.materialsDic[MaterialType.Fe_OH_3];
                    solidObjContainer.SetSedimentInfo(sediment);
                }
            }
        }
        container.materialTypes.Remove(MaterialType.Fe_OH_2);
        container.materialTypes.Add(MaterialType.Fe_OH_3);
    }
    #endregion
}

public class ChemistryReaction
{
    // 反应物列表
    public List<MaterialClass> reactant = new List<MaterialClass>();
    // 生成物列表
    public List<MaterialClass> product = new List<MaterialClass>();
    // 反应物可溶解性
    public DissolutionSolid dissolutionSolid = new DissolutionSolid();
}

public class ChemistryReactionJson
{
    public List<MaterialType> reactantType = new List<MaterialType>();
    public List<MaterialType> productType = new List<MaterialType>();
    public DissolutionSolid dissolutionSolid = new DissolutionSolid();
}
public class DissolutionSolid
{
    public bool IsSoluble = false;
    public List<MaterialType> types = new List<MaterialType>();
}

