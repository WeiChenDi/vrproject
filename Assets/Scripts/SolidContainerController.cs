﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidContainerController : MonoBehaviour
{
    public MaterialType type;
    private MaterialClass _material;

    // Start is called before the first frame update
    void Start()
    {
        MaterialConfigMgr.Instance.materialsDic.TryGetValue(type, out _material);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
