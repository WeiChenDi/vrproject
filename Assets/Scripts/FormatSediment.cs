﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FormatSediment
{
    private List<GameObject> _sedimentsPrefabList = new List<GameObject>();
    public List<GameObject> SedimentPrefabList
    {
        get
        {
            return _sedimentsPrefabList;
        }
    }
    private const string _path = "Prefabs/Rocks/";
    private const int _maxSediments = 6;
    private const int _minSediments = 3;
    public static MonoBehaviour Mono;

    private static FormatSediment _instance;
    public static FormatSediment Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new FormatSediment();
            }
            return _instance;
        }
    }

    // Start is called before the first frame update
    public FormatSediment()
    {
        Object[] objs = Resources.LoadAll(_path);
        for(int i = 0; i < objs.Length; i++)
        {
            _sedimentsPrefabList.Add((GameObject)objs[i]);
        }
    }

    public void OnFormatSediment(MaterialClass material, ContainerController container)
    {
        Transform containerTsm = container.transform;
        Vector3 containerMeshSize;
        GetContainerMeshSize(out containerMeshSize, containerTsm);
        containerMeshSize.y = GetLiquidLevel(containerTsm, containerMeshSize.y);
        int sedimentCnt = Random.Range(_minSediments, _maxSediments);
        List<GameObject> sedimentObjList = new List<GameObject>();
        for(int i = 0; i < sedimentCnt; i++)
        {
            // float time = Random.Range(0f, (float)ChemicalReactioinHandler.Instance.ReactTime - 2);
            Coroutine coroutine;
            coroutine = Mono.StartCoroutine(FormatSedimentCoroutine(container, containerMeshSize, material));
        }
    }

    IEnumerator FormatSedimentCoroutine(ContainerController container, Vector3 containerMeshSize, MaterialClass material)
    {
        while(true)
        {
            yield return new WaitForSeconds(0.1f);
            int sedimentType = Random.Range(0, 9);
            GameObject sedimentObj = GameObject.Instantiate(_sedimentsPrefabList[sedimentType], container.transform);
            sedimentObj.transform.position = GetSedimentObjPosition(containerMeshSize, container.transform.position);
            Sediment sediment = new Sediment();
            sediment.sedimentObj = sedimentObj;
            Mono.StartCoroutine(UpScale(true, sediment.sedimentObj));
            sediment.material = material;
            container.SedimentsList.Add(sediment);
            SolidObjController solidObjController = sedimentObj.GetComponent<SolidObjController>();
            solidObjController.SetSedimentInfo(sediment);
            Material mat = ColorConfigMgr.Instance.SolidMatDic[sediment.material.color];
            sedimentObj.GetComponent<Renderer>().material = mat;
            yield break;
        }
    }

    IEnumerator UpScale(bool upScale, GameObject sedimentObj)
    {
        Vector3 initScale;
        Vector3 currentScale;
        Vector3 targetScale;
        const int framesCount = 100;
        float animationTimeSeconds = Random.Range(1.5f, ChemicalReactioinHandler.Instance.ReactTime - 2);
        float deltaTime = animationTimeSeconds / framesCount;
        if(upScale)
        {
            initScale = new Vector3(0, 0, 0);
            currentScale = initScale;
            targetScale = sedimentObj.transform.localScale;
            sedimentObj.transform.localScale = currentScale;
        }
        else
        {
            initScale = sedimentObj.transform.localScale;
            currentScale = initScale;
            targetScale = new Vector3(0, 0, 0);
            sedimentObj.transform.localScale = currentScale;
        }
        Vector3 deltaScale = new Vector3(0, 0, 0);
        deltaScale.x = (targetScale.x - initScale.x) / (float)framesCount;
        deltaScale.y = (targetScale.y - initScale.y) / (float)framesCount;
        deltaScale.z = (targetScale.z - initScale.z) / (float)framesCount;
        while (true)
        {
            while(upScale)
            {
                currentScale += deltaScale;
                if(currentScale.x >= targetScale.x && currentScale.y >= targetScale.y && currentScale.z >= targetScale.z)
                {
                    yield break;
                }
                sedimentObj.transform.localScale = currentScale;
                yield return new WaitForSeconds(deltaTime);
            }

            while (!upScale)
            {
                currentScale += deltaScale;
                if (currentScale.x <= targetScale.x && currentScale.y <= targetScale.y && currentScale.z <= targetScale.z)
                {
                    yield break;
                }
                sedimentObj.transform.localScale = currentScale;
                yield return new WaitForSeconds(deltaTime);
            }
        }
    }

    private Vector3 GetSedimentObjPosition(Vector3 containerMeshSize, Vector3 containerTsmPos)
    {
        Vector3 result = new Vector3();
        bool flag = false;
        while(!flag)
        {
            result.x = Random.Range(-containerMeshSize.x / 4, containerMeshSize.x / 4);
            result.z = Random.Range(-containerMeshSize.z / 4, containerMeshSize.z / 4);
            result.y = Random.Range(containerMeshSize.y / 4, containerMeshSize.y);
            float dis = Mathf.Sqrt(Mathf.Pow(result.x, 2) + Mathf.Pow(result.z, 2));
            if(dis >= containerMeshSize.x / 2)
            {
                continue;
            }
            else
            {
                flag = true;
            }
        }
        return result + containerTsmPos;
    }

    private float GetLiquidLevel(Transform containerTsm, float containerHeight)
    {
        LiquidVolumeFX.LiquidVolume volumn = (LiquidVolumeFX.LiquidVolume)containerTsm.GetComponentInChildren(typeof(LiquidVolumeFX.LiquidVolume));
        return volumn.level * containerHeight;
    }

    private void GetContainerMeshSize(out Vector3 meshSize, Transform containerTsm)
    {
        MeshFilter meshFilter = containerTsm.GetComponent<MeshFilter>();
        meshSize = new Vector3();
        meshSize.x = meshFilter.mesh.bounds.size.x * containerTsm.localScale.x;
        meshSize.y = meshFilter.mesh.bounds.size.y * containerTsm.localScale.y;
        meshSize.z = meshFilter.mesh.bounds.size.z * containerTsm.localScale.z;
    }

    public static Coroutine getKey(Dictionary<Coroutine, bool> dict, int index)
    {
        Coroutine k = default(Coroutine);
        int sum = 0;
        foreach (Coroutine key in dict.Keys)
        {
            sum++;
            if (sum == (index + 1))
                return key;
        }
        return k;
    }

    public static bool getValue(Dictionary<Coroutine, bool> dict, int index)
    {
        bool v = default(bool);
        int sum = 0;
        foreach (bool value in dict.Values)
        {
            sum++;
            if (sum == (index + 1))
                return value;
        }
        return v;
    }
}

public class Sediment
{
    public MaterialClass material;
    public GameObject sedimentObj;
}
