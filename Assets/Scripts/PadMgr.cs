﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadMgr : MonoBehaviour
{

    #region Variables
    private List<PadController> _padControllersList = new List<PadController>();
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Start()
    {
        int num = transform.childCount;
        for(int i = 0; i < num; i++)
        {
            PadController padController = transform.GetChild(i).GetComponent<PadController>();
            _padControllersList.Add(padController);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!GameMgr.Instance.IsOver)
        {
            int i = 0; 
            for (i = 0; i < _padControllersList.Count; i++)
            {
                if(!_padControllersList[i].Placed)
                {
                    break;
                }
            }
            if(i == _padControllersList.Count)
            {
                bool check = CheckCorrectness();
                // 结果正确
                if(check)
                {
                    SetAllCorrect();
                    GameMgr.Instance.SetGameOver();
                }
                else
                {
                    SetAllError();
                }
            }
        }
    }
    #endregion

    #region Custom Methods
    public bool CheckCorrectness()
    {
        for(int i = 0; i < _padControllersList.Count; i++)
        {
            if(_padControllersList[i].Type != _padControllersList[i].CurrentPlacedType)
            {
                return false;
            }
        }
        return true;
    }

    private void SetAllCorrect()
    {
        for(int i = 0; i < _padControllersList.Count; i++)
        {
            _padControllersList[i].SetCorrectMaterial();
        }
    }

    private void SetAllError()
    {
        for (int i = 0; i < _padControllersList.Count; i++)
        {
            _padControllersList[i].SetErrorMaterial();
        }
    }
    #endregion
}
