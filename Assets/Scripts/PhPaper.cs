﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class PhPaper : MonoBehaviour
{
    static GameObject m_singlePaperPref;
    public static GameObject SingPaperPref
    {
        get
        {
            if (m_singlePaperPref == null)
            {
                m_singlePaperPref = Resources.Load("Prefabs/SinglePaper") as GameObject;
            }
            return Instantiate(m_singlePaperPref);
        }
    }

    // PH 1~13
    static Color[] m_colors = { new Color(0.95f, 0.4f, 0.14f), new Color(0.96f, 0.93f, 0.11f), new Color(0.52f, 0.76f, 0.25f), new Color(0.2f, 0.66f, 0.29f),
                                new Color(0.04f, 0.72f, 0.71f), new Color(0.22f, 0.33f, 0.64f), new Color(0.39f, 0.27f, 0.62f)};

    static Material m_paperMat = null;

    static Dictionary<MaterialType, int> m_phDict = new Dictionary<MaterialType, int>();

    static int maxPh = 13;
    static int minPh = 1;

    Renderer m_renderer;
    Coroutine m_async;

    private void Awake()
    {
        // 物质酸碱性，需要更改物质配置文件的结构...这里暂时用一个字典替代
        if(m_phDict.Count == 0)
        {
            m_phDict.Add(MaterialType.HCl, 1);
            m_phDict.Add(MaterialType.CH3COOH, 3);
        }
    }
    void Start()
    {
        if (!m_paperMat)
            m_paperMat = Resources.Load("phPaperMat") as Material;

        m_renderer = GetComponent<MeshRenderer>();

        Material newMat = new Material(m_paperMat);
        newMat.color = GetColorByPh(7);
        m_renderer.material = newMat;

        m_async = null;
    }

    public void PhTest(List<MaterialType> materialTypes)
    {
        int ph = 7;
        int acid = 0;
        foreach(MaterialType type in materialTypes)
        {
            if(m_phDict.ContainsKey(type))
            {
                int curPh = m_phDict[type];
                if (curPh < 7)
                {
                    if (acid++ < 0)
                        Debug.LogError("酸碱共存");
                }
                else if(curPh > 7)
                {
                    if (acid-- > 0)
                        Debug.LogError("酸碱共存");
                }

                ph = (acid > 0) ? Mathf.Min(ph, curPh) : Mathf.Max(ph, curPh);

            }
        }
        ChangeColor(ph);
    }

    void ChangeColor(int phValue)
    {
        Color targetCol = GetColorByPh(phValue);
        if(m_async != null)
        {
            StopCoroutine(m_async);
        }
        m_async = StartCoroutine(ColorFading(targetCol));
    }

    Color GetColorByPh(int phValue)
    {
        phValue = Mathf.Clamp(phValue, minPh, maxPh);
        float idx = Mathf.Floor((float)(phValue - minPh) / (maxPh - minPh) * (m_colors.Length - 1));
        return m_colors[(int)idx];
    }

    IEnumerator ColorFading(Color targetCol)
    {
        float timer = 0;
        float fadeTime = 0.5f;

        Color deltaCol = (targetCol - m_renderer.material.color) / fadeTime;

        while((timer += Time.deltaTime) < fadeTime)
        {
            m_renderer.material.color += deltaCol * Time.deltaTime;
            yield return null;
        }

        yield return null;
    }
}
