﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissolveSediment
{
    public MonoBehaviour Mono;

    private static DissolveSediment _instance;
    public static DissolveSediment Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new DissolveSediment();
            }
            return _instance;
        }
    }
     
    public void OnDissolveSediment(List<MaterialType> dissolvableTypes, ContainerController container)
    {
        List<Sediment> sedimentToDelete = new List<Sediment>();
        for(int i = 0; i < container.SedimentsList.Count; i++)
        {
            if(dissolvableTypes.Contains(container.SedimentsList[i].material.type))
            {
                Coroutine coroutine;
                coroutine = Mono.StartCoroutine(DissolveSedimentCoroutine(i, container.SedimentsList[i].sedimentObj));
                sedimentToDelete.Add(container.SedimentsList[i]);
            }
        }
        for(int i = 0; i < sedimentToDelete.Count; i++)
        {
            if(container.SedimentsList.Contains(sedimentToDelete[i]))
            {
                container.SedimentsList.Remove(sedimentToDelete[i]);
            }
        }
    }

    IEnumerator DissolveSedimentCoroutine(int num, GameObject sedimentObj)
    {
        while(true)
        {
            yield return new WaitForSeconds(0.1f);
            Mono.StartCoroutine(DownScale(true, sedimentObj));
            yield break;
        }
    }

    IEnumerator DownScale(bool downScale, GameObject sedimentObj)
    {
        Vector3 initScale;
        Vector3 currentScale;
        Vector3 targetScale;
        const int framesCount = 100;
        float animationTimeSeconds = Random.Range(1.5f, ChemicalReactioinHandler.Instance.ReactTime - 2);
        float deltaTime = animationTimeSeconds / framesCount;
        initScale = sedimentObj.transform.localScale;
        currentScale = initScale;
        targetScale = new Vector3(0, 0, 0);
        sedimentObj.transform.localScale = currentScale;
        Vector3 deltaScale = new Vector3(0, 0, 0);
        deltaScale.x = (targetScale.x - initScale.x) / (float)framesCount;
        deltaScale.y = (targetScale.y - initScale.y) / (float)framesCount;
        deltaScale.z = (targetScale.z - initScale.z) / (float)framesCount;
        while(true)
        {
            currentScale += deltaScale;
            if (currentScale.x <= targetScale.x && currentScale.y <= targetScale.y && currentScale.z <= targetScale.z)
            {
                yield break;
            }
            sedimentObj.transform.localScale = currentScale;
            yield return new WaitForSeconds(deltaTime);
        }
    }
}
