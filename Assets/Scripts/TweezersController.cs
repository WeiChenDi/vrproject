﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweezersController : MonoBehaviour
{
    private GameObject _targetPoint;
    private TargetPointCollisionDetect _targetPointCollisionDetect;
    private bool _clipped = false;
    public bool Clipped
    {
        get
        {
            return _clipped;
        }
    }
    private Sediment _clippedSediment;
    private PhPaper _clippedPhPaper;
    private Rigidbody _clippedRigid;

    private GameObject _leftController;
    private GameObject _rightController;
    private Collider _leftCollider;
    private Collider _rightCollider;
    private GameObject _curController;
    private SteamVR_TrackedObject _leftTrackObj;
    private SteamVR_Controller.Device LeftController
    {
        get
        {
            if(_leftController != null)
            {
                return SteamVR_Controller.Input((int)_leftTrackObj.index);
            }
            else
            {
                return null;
            }
        }
    }
    private SteamVR_TrackedObject _rightTrackObj;
    private SteamVR_Controller.Device RightController
    {
        get
        {
            if(_rightController != null)
            {
                return SteamVR_Controller.Input((int)_rightTrackObj.index);
            }
            else
            {
                return null;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Transform cameraRig = GameObject.Find("[CameraRig]").transform;
        _leftController = cameraRig.Find("Controller (left)").gameObject;
        _rightController = cameraRig.Find("Controller (right)").gameObject;
        _leftCollider = _leftController.GetComponent<Collider>();
        _rightCollider = _rightController.GetComponent<Collider>();
        _leftTrackObj = _leftController.GetComponent<SteamVR_TrackedObject>();
        _rightTrackObj = _rightController.GetComponent<SteamVR_TrackedObject>();
        _targetPoint = transform.Find("TargetPoint").gameObject;
        _targetPointCollisionDetect = _targetPoint.GetComponent<TargetPointCollisionDetect>();
    }

    // Update is called once per frame
    void Update()
    {
        if(_clippedSediment != null)
        {
            _clippedSediment.sedimentObj.transform.position = _targetPoint.transform.position;
            _clippedSediment.sedimentObj.transform.rotation = _targetPoint.transform.rotation;
            // _clippedRigid.freezeRotation = true;
        }
        if(_clippedPhPaper != null)
        {
            _clippedPhPaper.gameObject.transform.position = _targetPoint.transform.position;
            _clippedPhPaper.gameObject.transform.rotation = _targetPoint.transform.rotation;
        }
        GetInput();
    }

    private void OnClipSedimentObject(GameObject targetObj)
    {
        SolidObjController tmpSolidObjController = targetObj.GetComponent<SolidObjController>();
        if(tmpSolidObjController == null)
        {
            Debug.LogError("Sediment need Component SolidObjController!!!");
        }
        targetObj.transform.position = _targetPoint.transform.position;
        _clippedRigid = targetObj.GetComponent<Rigidbody>();
        _clipped = true;
        _clippedSediment = tmpSolidObjController.SedimentInfo;
        _clippedSediment.sedimentObj = targetObj;
    }

    private void OnClipSolidContainer(GameObject targetContainerObj)
    {
        _clipped = true;
        int index = Random.Range(0, FormatSediment.Instance.SedimentPrefabList.Count);
        SolidContainerController solidContainer = targetContainerObj.GetComponent<SolidContainerController>();
        MaterialType type = solidContainer.type;
        MaterialClass materialClass;
        MaterialConfigMgr.Instance.materialsDic.TryGetValue(type, out materialClass);
        GameObject sedimentObj = Instantiate(FormatSediment.Instance.SedimentPrefabList[index]);
        sedimentObj.transform.position = _targetPoint.transform.position;
        SolidObjController sedimentController = sedimentObj.GetComponent<SolidObjController>();
        Sediment sediment = new Sediment();
        sediment.material = materialClass;
        sediment.sedimentObj = sedimentObj;
        sedimentController.SetSedimentInfo(sediment);
        sedimentObj.GetComponent<Renderer>().material = ColorConfigMgr.Instance.SolidMatDic[materialClass.color];
        _clippedSediment = sedimentController.SedimentInfo;
        _clippedRigid = _clippedSediment.sedimentObj.GetComponent<Rigidbody>();
    }

    private void OnClipPhIndicatorPaper(GameObject target)
    {
        _clipped = true;
        GameObject singlePaper = PhPaper.SingPaperPref;
        singlePaper.transform.position = _targetPoint.transform.position;
        _clippedRigid = singlePaper.GetComponent<Rigidbody>();
        _clippedPhPaper = singlePaper.GetComponent<PhPaper>();
        singlePaper.GetComponent<CustomGravity>().enabled = false;
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Controller (left)")
        {
            _curController = _leftController;
        }
        else if (other.gameObject.name == "Controller (right)")
        {
            _curController = _rightController;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // _curController = null;
    }

    private void GetInput()
    {
        if(_curController == _leftController && _leftController != null)
        {
            if (LeftController.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad) && _clipped == true)
            {
                _clipped = false;
                if (_clippedSediment != null)
                {
                    if (_clippedSediment.sedimentObj.transform.parent == transform)
                    {
                        _clippedSediment.sedimentObj.transform.parent = null;
                    }
                    _clippedSediment = null;
                    _clippedRigid.velocity = new Vector3(0, 0, 0);
                }
                if (_clippedPhPaper != null)
                {
                    _clippedPhPaper.GetComponent<CustomGravity>().enabled = true;
                    _clippedPhPaper = null;
                    _clippedRigid.velocity = new Vector3(0, 0, 0);
                }
            }
            else if(LeftController.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad) && _clipped == false)
            {
                if (_targetPointCollisionDetect.TouchedSediment)
                {
                    OnClipSedimentObject(_targetPointCollisionDetect.TouchedSedimentCollider.gameObject);
                }
                else if(_targetPointCollisionDetect.TouchedSolidContainer)
                {
                    OnClipSolidContainer(_targetPointCollisionDetect.TouchedSolidContainerCollider.gameObject);
                }
                else if(_targetPointCollisionDetect.ToucherPhIndicatorPaper)
                {
                    OnClipPhIndicatorPaper(_targetPointCollisionDetect.ToucherPhIndicatorPaperCollider.gameObject);
                }
            }
        }
        if(_curController == _rightController && _rightController != null)
        {
            if (RightController.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad) && _clipped == true)
            {
                _clipped = false;
                if (_clippedSediment != null)
                {
                    if (_clippedSediment.sedimentObj.transform.parent == transform)
                    {
                        _clippedSediment.sedimentObj.transform.parent = null;
                    }
                    _clippedSediment = null;
                    _clippedRigid.velocity = new Vector3(0, 0, 0);
                }
                if (_clippedPhPaper != null)
                {
                    _clippedPhPaper.GetComponent<CustomGravity>().enabled = true;
                    _clippedPhPaper = null;
                    _clippedRigid.velocity = new Vector3(0, 0, 0);
                }
            }
            else if (RightController.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad) && _clipped == false)
            {
                if (_targetPointCollisionDetect.TouchedSediment)
                {
                    OnClipSedimentObject(_targetPointCollisionDetect.TouchedSedimentCollider.gameObject);
                }
                else if (_targetPointCollisionDetect.TouchedSolidContainer)
                {
                    OnClipSolidContainer(_targetPointCollisionDetect.TouchedSolidContainerCollider.gameObject);
                }
                else if (_targetPointCollisionDetect.ToucherPhIndicatorPaper)
                {
                    OnClipPhIndicatorPaper(_targetPointCollisionDetect.ToucherPhIndicatorPaperCollider.gameObject);
                }
            }
        }
    }
}
