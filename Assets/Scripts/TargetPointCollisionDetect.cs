﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPointCollisionDetect : MonoBehaviour
{
    #region Variable
    private bool _touchedSediment = false;
    public bool TouchedSediment
    {
        get
        {
            return _touchedSediment;
        }
    }
    private bool _touchedSolidContainer;
    public bool TouchedSolidContainer
    {
        get
        {
            return _touchedSolidContainer;
        }
    }
    private bool _toucherPhIndicatorPaper;
    public bool ToucherPhIndicatorPaper
    {
        get
        {
            return _toucherPhIndicatorPaper;
        }
    }


    private Collider _touchedSedimentCollider;
    public Collider TouchedSedimentCollider
    {
        get
        {
            return _touchedSedimentCollider;
        }
    }
    private Collider _touchedSolidContainerCollider;
    public Collider TouchedSolidContainerCollider
    {
        get
        {
            return _touchedSolidContainerCollider;
        }
    }
    private Collider _toucherPhIndicatorPaperCollider;
    public Collider ToucherPhIndicatorPaperCollider
    {
        get
        {
            return _toucherPhIndicatorPaperCollider;
        }
    }

    private TweezersController _tweezersController;
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Start()
    {
        _tweezersController = transform.parent.GetComponent<TweezersController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Sediment")
        {
            _touchedSediment = true;
            _touchedSedimentCollider = other;
        }
        else if(other.tag == "SolidContainer")
        {
            _touchedSolidContainer = true;
            _touchedSolidContainerCollider = other;
        }
        else if(other.tag == "PhIndicatorPaper")
        {
            _toucherPhIndicatorPaper = true;
            _toucherPhIndicatorPaperCollider = other;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Sediment")
        {
            _touchedSediment = false;
            _touchedSedimentCollider = null;
        }
        else if(other.tag == "SolidContainer")
        {
            _touchedSolidContainer = false;
            _touchedSolidContainerCollider = null;
        }
        else if (other.tag == "PhIndicatorPaper")
        {
            _toucherPhIndicatorPaper = false;
            _toucherPhIndicatorPaperCollider = null;
        }
    }
    #endregion

}
