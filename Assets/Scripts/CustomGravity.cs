﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CustomGravity : MonoBehaviour
{
    public float gravityScale = 0.1f;

    Rigidbody m_rb;

    void Start()
    {
        m_rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if(m_rb)
        {
            Vector3 oppsiteGravity = 9.8f * (1 - gravityScale) * Vector3.up;
            m_rb.AddForce(oppsiteGravity, ForceMode.Acceleration);
        }
    }
}
