﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LiquidVolumeFX;

public class LiquidInContainerMgr : MonoBehaviour
{
    #region Variables
    private Dictionary<Collider, LiquidVolume> _containerLiquidDic = new Dictionary<Collider, LiquidVolume>();
    public Dictionary<Collider, LiquidVolume> ContainerLiquidDic
    {
        get
        {
            return _containerLiquidDic;
        }
        set
        {
            _containerLiquidDic = value;
        }
    }
    private static LiquidInContainerMgr _instance;
    public static LiquidInContainerMgr Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.Find("LiquidMgr").GetComponent<LiquidInContainerMgr>();
            }
            return _instance;
        }
    }
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    #endregion

    #region Custom Methods
    //public void InitContainerLiquid(List<Collider> colliders)
    //{
    //    for(int i = 0; i < colliders.Count; i++)
    //    {
    //        LiquidVolume liquidVolume;
    //        liquidVolume = colliders[i].transform.GetComponentInChildren<LiquidVolume>();
    //        if(liquidVolume == null)
    //        {
    //            Debug.Log("111");
    //        }
    //        _containerLiquidDic.Add(colliders[i], liquidVolume);
    //    }
    //}

    public void FillContainer(Component colliderComponent)
    {
        LiquidVolume liquidVolume;
        _containerLiquidDic.TryGetValue((Collider)colliderComponent, out liquidVolume);
        if(liquidVolume == null)
        {
            Debug.LogError("Can't find liquidVolume!!!");
        }
        else
        {
            liquidVolume.level += 0.0005f;
            Mathf.Clamp(liquidVolume.level, 0f, 1f);
        }
    }
    #endregion
}
