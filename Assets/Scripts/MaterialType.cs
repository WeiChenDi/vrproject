﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


// 使用一个MaterialType对应一个MaterialClass代表每种物质的种类和性质
public enum MaterialType
{
    None = 1000,
    BaNO3_2 = 1001,
    Na2SO4 = 1002,
    BaSO4 = 1003,
    HCl = 1004,
    Na2CO3 = 1005,
    NaNO3 = 1006,
    CO2 = 1007,
    BaCO3 = 1008,
    Cu2_OH_2CO3 = 1009,
    H2SO4 = 1010,
    NaOH = 1011,
    FeSO4_7H2O = 1012,
    CuSO4 = 1013,
    FeSO4 = 1014,
    Fe_OH_2 = 1015,
    Fe_OH_3 = 1016,
    Cu_OH_2 = 1017,
    H2O = 1018,
    Fe3_SO4_2 = 1019,

    
    // ADD BY BAI
    BaCl2 = 1020,
    K2SO4 = 1021,
    Phph = 1022, // 酚酞
    In = 1023, // 酚酞和碱产生In2-根呈粉红
    Mg_OH_2 = 1024,
    CH3COOH = 1025

    // TODO: 暂时无视若干无关产物
}

public enum MaterialState
{
    None = 1000,
    Liquid = 1001,
    Solid = 1002,
    Gas = 1003
}

public class MaterialClass
{
    public string name;
    public MaterialType type = MaterialType.None;
    public ColorType color;
    public MaterialState state = MaterialState.None;
    public bool solubleInAcid;
}

public class MaterialConfigMgr
{
    private static MaterialConfigMgr _instance;
    public static MaterialConfigMgr Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new MaterialConfigMgr();
            }
            return _instance;
        }
    }

    private MaterialConfigMgr()
    {
        InitConfig();
    }

    private List<MaterialClass> _materialsList = new List<MaterialClass>();
    private Dictionary<MaterialType, MaterialClass> _materialsDic = new Dictionary<MaterialType, MaterialClass>();
    public Dictionary<MaterialType, MaterialClass> materialsDic
    {
        get
        {
            return _materialsDic;
        }
    }

    private const string _configPath = "MaterialInfoConfig.json";
    public void InitConfig()
    {
        string filepath = Path.Combine(Application.streamingAssetsPath, _configPath);
        if(File.Exists(filepath))
        {
            string json = File.ReadAllText(filepath);
            _materialsList = JsonConvert.DeserializeObject<List<MaterialClass>>(json);
            for(int i = 0; i < _materialsList.Count; i++)
            {
                _materialsDic.Add(_materialsList[i].type, _materialsList[i]);
            }
        }
    }
}
