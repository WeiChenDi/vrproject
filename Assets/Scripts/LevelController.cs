﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    #region Variables
    private const string _reactionContainer = "ReactionContainer";
    public Level levelIndex;
    public Material PadNormalMat;
    public Material PadHighLightMat;
    private bool _placed = false;
    public bool Placed
    {
        get
        {
            return _placed;
        }
    }
    #endregion


    #region Main Methods
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == _reactionContainer)
        {
            SetHighLightMaterial();
            _placed = true;
            if(levelIndex == Level.LevelOne)
            {
                SceneManager.LoadScene("Room01");
            }
            else if(levelIndex == Level.LevelTwo)
            {
                SceneManager.LoadScene("Room02");
            }
            else if (levelIndex == Level.LevelThree)
            {
                SceneManager.LoadScene("Room03");
            }
            else if (levelIndex == Level.LevelFour)
            {
                SceneManager.LoadScene("Room04");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == _reactionContainer)
        {
            SetNormalMaterial();
            _placed = false;
        }
    }
    #endregion

    #region Custom Methods
    private void SetHighLightMaterial()
    {
        GetComponent<Renderer>().material = PadHighLightMat;
    }

    private void SetNormalMaterial()
    {
        GetComponent<Renderer>().material = PadNormalMat;
    }
    #endregion

}
