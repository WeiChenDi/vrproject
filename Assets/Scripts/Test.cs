﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class Test : MonoBehaviour
{

    public GameObject beaker;
    public ObiSolver solver;
    private GameObject _beakerObj;
    private ObiFluidRenderer _fluidRenderer;

    // Start is called before the first frame update
    void Start()
    {
        _beakerObj = Instantiate(beaker);
        _fluidRenderer = GameObject.Find("Main Camera").GetComponent<ObiFluidRenderer>();
        ObiEmitter emitter = _beakerObj.transform.Find("Emitter").GetComponent<ObiEmitter>();
        ObiParticleRenderer renderer = _beakerObj.transform.Find("Emitter").GetComponent<ObiParticleRenderer>();
        if(emitter == null)
        {
            Debug.Log("*********");
        }
        for(int i = 0; i < _fluidRenderer.particleRenderers.Length; i++)
        {
            if(_fluidRenderer.particleRenderers[i] == null)
            {
                _fluidRenderer.particleRenderers[i] = renderer;
                break;
            }
        }
        emitter.Solver = solver;

        emitter.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
