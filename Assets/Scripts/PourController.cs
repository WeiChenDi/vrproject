﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;
using LiquidVolumeFX;

[RequireComponent(typeof(ObiEmitter))]
public class PourController : MonoBehaviour
{
    #region Variables
    private Transform _containerTsm;
    private ObiEmitter _emitter;
    private LiquidVolume _liquidVolume;
    private ContainerController _container;

    private const string _potionTag = "Potion";
    private const string _beakerTag = "ReactionContainer";
    private const float _beakerVolumeMinLevel = 0.2f;
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Start()
    {
        _containerTsm = transform.parent;
        _emitter = GetComponent<ObiEmitter>();
        _liquidVolume = _containerTsm.GetComponentInChildren<LiquidVolume>();
        _container = _containerTsm.GetComponent<ContainerController>();
    }

    // Update is called once per frame
    void Update()
    {
        Pouring();
    }
    #endregion

    #region Custom Methods
    private void Pouring()
    {
        float xAngle, zAngle;
        xAngle = Mathf.Abs(_containerTsm.rotation.x);
        zAngle = Mathf.Abs(_containerTsm.rotation.z);
        if(_containerTsm.tag == _potionTag)
        {
            if (xAngle > 0.5 || zAngle > 0.5)
            {
                _emitter.speed = (Mathf.Max(xAngle, zAngle) - 0.5f) * 10f;
            }
            else
            {
                _emitter.speed = 0f;
            }
        }
        else if (_containerTsm.tag == _beakerTag)
        {
            if (xAngle > 0.5 || zAngle > 0.5)
            {
                if(_liquidVolume.level > _beakerVolumeMinLevel)
                {
                    _emitter.speed = (Mathf.Max(xAngle, zAngle) - 0.5f) * 5f;
                    _liquidVolume.level -= 0.005f;
                }
                else
                {
                    _emitter.speed = 0f;
                    _container.materialTypes.Clear();
                }
            }
            else
            {
                _emitter.speed = 0f;
            }
        }
    }
    #endregion
}
