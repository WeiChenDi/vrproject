﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

public enum ColorType
{
    White = 1001,
    Green = 1002,
    Blue = 1003,
    Red_Brown = 1004,
    Pink = 1005
}

public class ColorConfigMgr
{
    private static ColorConfigMgr _instance;
    public static ColorConfigMgr Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new ColorConfigMgr();
            }
            return _instance;
        }
    }

    private const string _configPath = "ChemistryColorConfig.json";
    private const string _solidMatPath = "SolidMaterials/";
    private Dictionary<ColorType, ChemistryColor> _liquidColorDic = new Dictionary<ColorType, ChemistryColor>();
    private Dictionary<ColorType, ChemistryColor> _solidColorDic = new Dictionary<ColorType, ChemistryColor>();
    private Dictionary<ColorType, Material> _solidMatDic = new Dictionary<ColorType, Material>();
    public Dictionary<ColorType, Material> SolidMatDic
    {
        get
        {
            return _solidMatDic;
        }
    }

    public ColorConfigMgr()
    {
        InitConfig();
    }

    private void InitConfig()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, _configPath);
        if(File.Exists(filePath))
        {
            List<ChemistryColor> tmpColorList = new List<ChemistryColor>();
            string json = File.ReadAllText(filePath);
            tmpColorList = JsonConvert.DeserializeObject<List<ChemistryColor>>(json);
            for(int i = 0; i < tmpColorList.Count;i++)
            {
                if(tmpColorList[i].state == MaterialState.Liquid)
                {
                    _liquidColorDic.Add(tmpColorList[i].type, tmpColorList[i]);
                }
                else if(tmpColorList[i].state == MaterialState.Solid)
                {
                    _solidColorDic.Add(tmpColorList[i].type, tmpColorList[i]);
                }
            }
        }
        foreach(var value in _solidColorDic.Values)
        {
            string path = _solidMatPath + value.name;
            Material mat = Resources.Load(path, typeof(Material)) as Material;
            _solidMatDic.Add(value.type, mat);
        }
    }

    public Color GetColor(ColorType type, MaterialState state)
    {
        ChemistryColor chemistryColor = new ChemistryColor();
        if(state == MaterialState.Liquid)
        {
            _liquidColorDic.TryGetValue(type, out chemistryColor);
            if(chemistryColor == null)
            {
                Debug.LogError("No Match Color!!!");
                return Color.white;
            }
            Color color = new Color(chemistryColor.r, chemistryColor.g, chemistryColor.b, chemistryColor.a);
            return color;
        }
        else if(state == MaterialState.Solid)
        {
            _solidColorDic.TryGetValue(type, out chemistryColor);
            if (chemistryColor == null)
            {
                Debug.LogError("No Match Color!!!");
                return Color.white;
            }
            Color color = new Color(chemistryColor.r, chemistryColor.g, chemistryColor.b, chemistryColor.a);
            return color;
        }
        else
        {
            Debug.LogError("No Match State!");
            return Color.white;
        }
    }
}

public class ChemistryColor
{
    public string name;
    public ColorType type;
    public MaterialState state;
    public float r;
    public float g;
    public float b;
    public float a;
}
