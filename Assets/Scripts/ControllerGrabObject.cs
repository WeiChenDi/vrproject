﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerGrabObject : MonoBehaviour
{
    #region Variables
    private SteamVR_TrackedObject _trackObj;
    // 保存与手柄碰撞的触发器
    private GameObject _collidingObj;
    // 保存玩家抓住的对象
    private GameObject _objInHand;
    private SteamVR_Controller.Device Controller
    {
        get
        {
            return SteamVR_Controller.Input((int)_trackObj.index);
        }
    }
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Awake()
    {
        // 获取手柄备用
        _trackObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Controller.GetHairTriggerDown())
        {
            if (_collidingObj)
            {
                GrabObj();
            }
        }
        if(Controller.GetHairTriggerUp())
        {
            if(_objInHand)
            {
                ReleaseObject();
            }
        }
    }
    #endregion

    #region Custom Methods
    // 接收碰撞体作为参数，保存在collidingObj中
    private void SetCollidingObj(Collider col)
    {
        if(_collidingObj || !col.GetComponent<Rigidbody>())
        {
            return;
        }
        _collidingObj = col.gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        SetCollidingObj(other);
    }

    private void OnTriggerStay(Collider other)
    {
        SetCollidingObj(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if(!_collidingObj)
        {
            return;
        }
        _collidingObj = null;
    }

    // 将触碰到的物体转移到手中
    private void GrabObj()
    {
        _objInHand = _collidingObj;
        _collidingObj = null;
        var joint = AddFixedJoint();
        joint.connectedBody = _objInHand.GetComponent<Rigidbody>();
    }

    // 添加连接对象，将手柄与GameObject连接起来
    private FixedJoint AddFixedJoint()
    {
        FixedJoint fx = gameObject.AddComponent<FixedJoint>();
        fx.breakForce = 20000;
        fx.breakTorque = 20000;
        return fx;
    }

    // 放下一个物体
    // 将joint删除，并设置物体丢出的速度
    private void ReleaseObject()
    {
        if(GetComponent<FixedJoint>())
        {
            GetComponent<FixedJoint>().connectedBody = null;
            Destroy(GetComponent<FixedJoint>());
            _objInHand.GetComponent<Rigidbody>().velocity = Controller.velocity;
            _objInHand.GetComponent<Rigidbody>().angularVelocity = Controller.velocity;
        }
        _objInHand = null;
    }
    #endregion
}
