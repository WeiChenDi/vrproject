﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Level
{
    LevelOne = 1001,
    LevelTwo = 1002,
    LevelThree = 1003,
    LevelFour = 1004
}

public class GameMgr : MonoBehaviour
{
    #region Variables
    private bool _isOver = false;
    public bool IsOver
    {
        get
        {
            return _isOver;
        }
    }
    public Level levelIndex;

    private static GameMgr _instance;
    public static GameMgr Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.Find("GameMgr").GetComponent<GameMgr>();
            }
            return _instance;
        }
    }
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion

    #region Custom Methods
    public void SetGameOver()
    {
        _isOver = true;
        if (levelIndex == Level.LevelOne)
        {
            SceneManager.LoadScene("Room02");
        }
        else if (levelIndex == Level.LevelTwo)
        {
            SceneManager.LoadScene("Room03");
        }
        else if (levelIndex == Level.LevelThree)
        {
            SceneManager.LoadScene("Room04");
        }
        else if (levelIndex == Level.LevelFour)
        {
            SceneManager.LoadScene("Start");
        }
    }
    #endregion
}
