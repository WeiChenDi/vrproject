﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

[RequireComponent(typeof(ObiSolver))]
public class CollisionHandler : MonoBehaviour
{
    #region Variables
    private ObiSolver _solver;
    public List<Collider> killers;

    private static CollisionHandler _instance;
    public static CollisionHandler Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.Find("Solver").GetComponent<CollisionHandler>();
            }
            return _instance;
        }
    }
    #endregion

    #region Main Methods
    // Start is called before the first frame update
    void Awake()
    {
        _solver = GetComponent<ObiSolver>();
    }

    private void OnEnable()
    {
        _solver.OnCollision += SolverOnCollision;
    }

    private void Start()
    {
        //LiquidInContainerMgr.Instance.InitContainerLiquid(killers);
    }

    private void OnDisable()
    {
        _solver.OnCollision -= SolverOnCollision;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion

    #region Custom Methods
    private void SolverOnCollision(object sender, ObiSolver.ObiCollisionEventArgs e)
    {
        for(int i = 0; i < e.contacts.Count; i++)
        {
            if (e.contacts[i].distance < 0.1f)
            {
                Component colliderComponent;
                if(ObiCollider.idToCollider.TryGetValue(e.contacts[i].other, out colliderComponent))
                {
                    Collider collider = colliderComponent as Collider;
                    for (int j = 0; j < killers.Count; j++)
                    {
                        if (collider == killers[j])
                        {
                            ObiSolver.ParticleInActor pa = _solver.particleToActor[e.contacts[i].particle];
                            ObiEmitter sourceEmitter = pa.actor as ObiEmitter;
                            if (sourceEmitter != null)
                            {
                                if (sourceEmitter.transform.parent != collider.transform)
                                {
                                    ProcessCollidedLiquid(sourceEmitter, pa, collider);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void ProcessCollidedLiquid(ObiEmitter sourceEmitter, ObiSolver.ParticleInActor pa, Component collider)
    {
        sourceEmitter.life[pa.indexInActor] = 0;
        LiquidInContainerMgr.Instance.FillContainer(collider);
        GameObject srcContainerObj = sourceEmitter.transform.parent.gameObject;
        GameObject dstContainerObj = collider.gameObject;
        if(dstContainerObj.tag == "ReactionContainer")
        {
            ChemicalReactioinHandler.Instance.MaterialMixing(srcContainerObj, dstContainerObj);
        }
    }
    #endregion
}
