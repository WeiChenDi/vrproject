﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;

public class ChemistryReactionConfigMgr
{
    private static ChemistryReactionConfigMgr _instance;
    public static ChemistryReactionConfigMgr Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ChemistryReactionConfigMgr();
            }
            return _instance;
        }
    }

    private const string _configPath = "ChemistryReactionInfoConfig.json";

    public void InitConfig()
    {
        string filepath = Path.Combine(Application.streamingAssetsPath, _configPath);
        if (File.Exists(filepath))
        {
            List<ChemistryReactionJson> _chemistryRectionsJsonList = new List<ChemistryReactionJson>();
            string json = File.ReadAllText(filepath);
            _chemistryRectionsJsonList = JsonConvert.DeserializeObject<List<ChemistryReactionJson>>(json);
            for (int i = 0; i < _chemistryRectionsJsonList.Count; i++)
            {
                ChemistryReaction tmpReaction = new ChemistryReaction();
                for (int j = 0; j < _chemistryRectionsJsonList[i].reactantType.Count; j++)
                {
                    tmpReaction.reactant.Add(MaterialConfigMgr.Instance.materialsDic[_chemistryRectionsJsonList[i].reactantType[j]]);
                }
                for (int j = 0; j < _chemistryRectionsJsonList[i].productType.Count; j++)
                {
                    tmpReaction.product.Add(MaterialConfigMgr.Instance.materialsDic[_chemistryRectionsJsonList[i].productType[j]]);
                }
                tmpReaction.dissolutionSolid = _chemistryRectionsJsonList[i].dissolutionSolid;
                ChemicalReactioinHandler.Instance.chemistryReactionsList.Add(tmpReaction);
            }
        }
    }
}
