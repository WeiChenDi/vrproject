using UnityEditor;
using UnityEngine;
using System.IO;

namespace PrefabEvolution
{
	static class PEPrefs
	{
		static internal int DebugLevel = 0;
		private const string PrefsPrefix = "PrefabEvolution";

		static PEPrefs()
		{
			UpdateDebugLevel();
		}

		static void UpdateDebugLevel()
		{
			DebugLevel = DebugMode ? 1 : 0;
		}

		static string GetPrefName(string name)
		{
			return PrefsPrefix + name;
		}

		static internal bool AutoModels
		{
			get
			{
                return false;//强制为 false， Edit By Wangbenchong 2018/1/12
                //return EditorPrefs.GetBool(GetPrefName("AutoModels"), true);
            }

			private set
			{
                //if (AutoModels != value)//强制为 false， Edit By Wangbenchong 2018/1/12
                //	EditorPrefs.SetBool(GetPrefName("AutoModels"), value);
            }
        }

		static internal bool AutoPrefabs
		{
			get
			{
                return false; //强制为 false， Edit By Wangbenchong 2018/1/12
				//return EditorPrefs.GetBool(GetPrefName("AutoPrefabs"), false);
			}

			private set
			{
                //if (AutoPrefabs != value)//强制为 false， Edit By Wangbenchong 2018/1/12
                //	EditorPrefs.SetBool(GetPrefName("AutoPrefabs"), value);
            }
        }

		static internal bool DrawOnHeader
		{
			get
			{
				return EditorPrefs.GetBool(GetPrefName("DrawOnHeader"), true);
			}

			private set
			{
				if (DrawOnHeader != value)
					EditorPrefs.SetBool(GetPrefName("DrawOnHeader"), value);
			}
		}

		static internal bool AutoSaveAfterApply
		{
			get
			{
				return EditorPrefs.GetBool(GetPrefName("AutoSaveAfterApply"), false);
			}

			private set
			{
				if (AutoSaveAfterApply != value)
					EditorPrefs.SetBool(GetPrefName("AutoSaveAfterApply"), value);
			}
		}

		static internal bool DebugMode
		{
			get
			{
				return EditorPrefs.GetBool(GetPrefName("DebugMode"), false);
			}

			private set
			{
				if (DebugMode != value)
				{
					EditorPrefs.SetBool(GetPrefName("DebugMode"), value);
					UpdateDebugLevel();
				}
			}
		}

		static private Vector2 ScrollPos;

		#if TRIAL
		[PreferenceItem("Prefab Evolution Free")]
		#else
		[PreferenceItem("Prefab Evolution")]
		#endif
		static void OnPrefsGUI()
		{
			ScrollPos = EditorGUILayout.BeginScrollView(ScrollPos);
			EditorGUILayout.HelpBox("==Locked==Automaticly add \"EvolvePrefab\" script to models on import. ", MessageType.Info);
			AutoModels = EditorGUILayout.Toggle("Make imported models nested", AutoModels);

			EditorGUILayout.HelpBox("==Locked==Automaticly add \"EvolvePrefab\" script to new prefab", MessageType.Info);
			AutoPrefabs = EditorGUILayout.Toggle("Make prefab nested on create", AutoPrefabs);

			EditorGUILayout.HelpBox("Show utility buttons on GameObject Inspector header", MessageType.Info);
			DrawOnHeader = EditorGUILayout.Toggle("Show in header", DrawOnHeader);

			EditorGUILayout.HelpBox("Force save all Assets after applying changes to prefab", MessageType.Info);
			AutoSaveAfterApply = EditorGUILayout.Toggle("Auto save", AutoSaveAfterApply);

			EditorGUILayout.HelpBox("Plugin will write detailed log", MessageType.Info);
			DebugMode = EditorGUILayout.Toggle("Debug mode", DebugMode);

			EditorGUILayout.HelpBox("Check all prefabs", MessageType.Info);
			if (GUILayout.Button("Check prefab dependencies"))
				PECache.ForceCheckAllAssets();

            EditorGUILayout.HelpBox("Remove EvolvePrefab Script in Select Folder", MessageType.Info);
            Folder = EditorGUILayout.ObjectField("Folder:", Folder, typeof(Object), true);
            if (GUILayout.Button("Remove EvolvePrefab Script"))
                RemovePeScript();
            EditorGUILayout.EndScrollView();
		}
        static Object Folder;
        private static void RemovePeScript()
        {
            if(Folder == null)
            {
                EditorUtility.DisplayDialog("Error", "Folder is null", "ok");
                return;
            }
            string fullPath = Application.dataPath.Replace("/Assets","/") + AssetDatabase.GetAssetPath(Folder);
            fullPath = fullPath.Replace("/", @"\");
            if(!Directory.Exists(fullPath))
            {
                EditorUtility.DisplayDialog("Error", fullPath + " is not a folder Directory", "ok");
                return;
            }
            string[] files = System.IO.Directory.GetFiles(fullPath, "*.prefab", SearchOption.AllDirectories);
            int filesLength = files.Length;
            int changeCount = 0;
            for (int i = 0; i < filesLength; i++)
            {
                bool changed = false;
                string assetName = files[i];
                EditorUtility.DisplayProgressBar("Checking Model Prefab", assetName, i * 1f / filesLength);
                assetName = "Assets" + assetName.Substring(Application.dataPath.Length, assetName.Length - Application.dataPath.Length);
                GameObject objRes = AssetDatabase.LoadMainAssetAtPath(assetName) as GameObject;

                EvolvePrefab[] peObjs = objRes.GetComponentsInChildren<EvolvePrefab>(true);
                for (int j = 0; j < peObjs.Length; ++j)
                {
                    EvolvePrefab current = peObjs[j];
                    if (current == null)
                    {
                        continue;
                    }
                    GameObject.DestroyImmediate(current, true);
                    if (!changed)
                    {
                        changed = true;
                        changeCount++;
                    }
                }
                if (changed)
                {
                    EditorUtility.SetDirty(objRes);
                }
            }
            AssetDatabase.Refresh();
            EditorUtility.ClearProgressBar();
            EditorUtility.DisplayDialog("Over", "Total Edit " + changeCount + " prefab, use Ctrl+S to save Edit, then you can commit", "ok");
        }
    }
}